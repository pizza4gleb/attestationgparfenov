import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FormatTimeTest {

    @Test
    public void correctDateIsConverted() {
        String initialDate = TestHelper.dateConverter("22/09/2009");
        assertEquals(initialDate, "2009/09/22");
    }

    @Test
    public void dateWithoutYearReturnsError() {
        String initialDate = TestHelper.dateConverter("45/34");
        assertEquals(initialDate, "Parse error!");
    }

    @Test
    public void OnlyYearReturnsError() {
        String initialDate = TestHelper.dateConverter("1994");
        assertEquals(initialDate, "Parse error!");
    }

    @Test
    public void YearAndMonthReturnsError() {
        String initialDate = TestHelper.dateConverter("07/1994");
        assertEquals(initialDate, "Parse error!");
    }

    @Test
    public void DayAndYearReturnsError() {
        String initialDate = TestHelper.dateConverter("22//2010");
        assertEquals(initialDate, "Parse error!");
    }

    @Test
    public void EmptyStringReturnsError() {
        String initialDate = TestHelper.dateConverter("");
        assertEquals(initialDate, "Parse error!");
    }

}
