import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class RandomYearTest {

    @Test
    public void correctValueForRandomizer() {
        int randomYear = TestHelper.randomizeYear();
        int lowestBoundary = 1969;
        int highestBoundary = 2022;
        boolean checkIsTrue = lowestBoundary < randomYear && highestBoundary > randomYear;
        assertTrue(checkIsTrue);
    }
}
