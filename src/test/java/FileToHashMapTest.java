import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class FileToHashMapTest {
    @Test
    void mapContainValuesFromFile() throws IOException {
        Map<String, String> myMap = TestHelper.fileDataToHashMap();
        assertTrue(myMap.containsKey("foo") && myMap.get("foo") != null);
    }
}
