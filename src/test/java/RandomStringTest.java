import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class RandomStringTest {
    @Test
    public void checkStringPattern() {
        String pattern = "([A-Z]\\w+)\\s([A-Z]\\w+)\\s([A-Z]\\w+)\\s";
        String randomString = TestHelper.generateRandomString(5);
        assertTrue(randomString.matches(pattern));
    }
}
