import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StringToDoubleTest {

    @Test
    public void correctStringToDouble() {
        double initialString = TestHelper.stringToDouble("832.94");
        assertEquals(initialString, 832.94);
    }

    @Test
    public void incorrectStringToDouble() {
        double initialString = TestHelper.stringToDouble("qwerty");
        assertEquals(initialString, Double.POSITIVE_INFINITY);
    }
}