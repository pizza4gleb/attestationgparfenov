import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RandomLongValueTest {
    @Test
    public void checkRandomValueIsLong() {
        long randomLong = TestHelper.randomizeLong();
        Long notPrimitiveLong = Long.valueOf(randomLong);
        assertEquals(randomLong, notPrimitiveLong);
    }
}
