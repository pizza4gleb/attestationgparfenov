import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public abstract class TestHelper {

    private static final Random random = new Random();

    public static long randomizeLong() {
        return randomizeLongEncapsulated();
    }

    public static int randomizeYear() {
        return randomizeYearEncapsulated();
    }

    public static String generateRandomString(int length) {
        return generateRandomStringEncapsulated(length);
    }

    public static double stringToDouble(String s) {
        return stringToDoubleEncapsulated(s);
    }

    public static Map<String, String> fileDataToHashMap() throws IOException {
        return fileDataToHashMapEncapsulated();
    }

    /**
     * Метод принимает в себя строку в формате dd/MM/yyyy,
     * И возвращает конвертированную дату в формате yyyy/MM/dd
     */
    public static String dateConverter(String stringDate) {
        return dateConverterEncapsulated(stringDate);
    }

    private static long randomizeLongEncapsulated() {
        return random.nextLong();
    }

    private static int randomizeYearEncapsulated() {
        int sinceYear = 1970;
        int nowYear = 2021;
        return random.nextInt(nowYear - sinceYear) + sinceYear;
    }

    private static String generateRandomStringEncapsulated(int length) {
        String strPattern = "abcdefghijklmnopqrstuvwxyz";
        String randomString = "";

        if (length < 1) throw new IllegalArgumentException();

        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < length; j++) {

                int rndCharAt = random.nextInt(strPattern.length());
                char rndChar = strPattern.charAt(rndCharAt);

                sb.append(rndChar);
            }

            String randomWord = sb.substring(0, 1).toUpperCase() + sb.substring(1);
            randomString += randomWord + " ";
        }
        return randomString;
    }

    private static double stringToDoubleEncapsulated(String string) {
        double infinity = Double.POSITIVE_INFINITY;

        try {
            return Double.parseDouble(string);
        } catch (Exception e) {
            return infinity;
        }
    }

    private static Map<String, String> fileDataToHashMapEncapsulated() throws IOException {
        String filePath = "C:\\Users\\parfe\\Desktop\\test123.txt";
        Map<String, String> map = new HashMap<>();
        String line;
        BufferedReader reader = new BufferedReader(new FileReader(filePath));

        while ((line = reader.readLine()) != null) {
            String[] parts = line.split("::", 2);
            if (parts.length >= 2) {
                String key = parts[0];
                String value = parts[1];
                map.put(key, value);
            } else {
                System.out.println("ignoring line: " + line);
            }
        }
        reader.close();
        return map;
    }

    private static String dateConverterEncapsulated(String stringDate) {
        String dayMonthYear = "dd/MM/yyyy";
        String yearMonthDay = "yyyy/MM/dd";

        SimpleDateFormat sdf = new SimpleDateFormat(dayMonthYear);
        Date d;
        try {
            d = sdf.parse(stringDate);
        } catch (ParseException e) {
            return "Parse error!";
        }
        sdf.applyPattern(yearMonthDay);
        return sdf.format(d);
    }

}
